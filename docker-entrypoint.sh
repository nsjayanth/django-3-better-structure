#!/bin/sh
set -e

# until psql $DATABASE_URL -c '\l'; do
#   >&2 echo "Postgres is unavailable - sleeping"
#   sleep 1
# done

echo >&2 "sqlite is up - continuing"
python manage.py collectstatic --noinput
python manage.py migrate --noinput

exec "$@"
