from typing import Any, Dict, List
from django.core.management.base import BaseCommand
from django.utils import timezone

from type_checkings import start_type_checking


class Command(BaseCommand):
    help = "Check for typings"

    def handle(self, *args: List[str], **options: Dict[str, Any]) -> int:
        start_type_checking()
