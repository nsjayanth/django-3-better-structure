from rest_framework import serializers


def valid_password_pattern(password: str) -> str:
    """
    function to validate the password pattern
    """
    import re
    password_pattern = r'[A-Za-z0-9@#$%^&+=]{8,16}'
    if re.fullmatch(password_pattern, password):
        return password
    raise serializers.ValidationError("Password must be between 8 to 16 characters.")
