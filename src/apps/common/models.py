from django.db import models
from django.utils.translation import ugettext_lazy as _


# DB models

class BaseCustomModel(models.Model):  # type: ignore
    """A base abstract model to provide the datetime fields (created and updated)for the tables
    """
    created_at = models.DateTimeField(_("created at"), auto_now=False, auto_now_add=True)
    updated_at = models.DateTimeField(_("updated at"), auto_now=True, auto_now_add=False)

    class Meta:
        abstract = True
