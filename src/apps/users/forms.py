from django.contrib.auth import models
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):  # type: ignore

    class Meta:
        model = CustomUser
        fields = ('email',)


class CustomUserChangeForm(UserChangeForm):  # type: ignore

    class Meta:
        model = CustomUser
        fields = ('email',)
