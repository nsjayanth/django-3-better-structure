from typing import Any, Dict
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import CustomUser as User

from apps.common.validators import valid_password_pattern


class UserSerializer(serializers.ModelSerializer):  # type: ignore
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(min_length=8, max_length=16, write_only=True,
                                     validators=[valid_password_pattern])

    class Meta:
        model = User
        fields = ('id', 'email', 'password')

    def create(self, validated_data: Dict[str, Any]) -> User:
        user = User.objects.create_user(**validated_data)
        return user


class UserResponseSerializer(UserSerializer):
    class Meta:
        model = User
        fields = ('id', "email")


class LoginSerializer(serializers.Serializer):  # type: ignore
    email = serializers.EmailField()
    password = serializers.CharField()

    class Meta:
        model = User
        fields = ('email', 'password')


class TokenSerializer(serializers.Serializer):  # type: ignore
    token = serializers.CharField(read_only=True)
