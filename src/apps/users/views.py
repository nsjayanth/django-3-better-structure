from __future__ import unicode_literals

import logging
from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


from .serializers import LoginSerializer, TokenSerializer, UserSerializer, UserResponseSerializer

LOG = logging.getLogger(__name__)

# Get the JWT settings, add these lines after the import/from lines
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

user_response = openapi.Response('response description', UserResponseSerializer)
login_response = openapi.Response('login response', TokenSerializer)

# Create your views here.


class CreateUserAPIView(APIView):  # type: ignore
    # Allow any user (authenticated or not) to access this url
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    @swagger_auto_schema(request_body=UserSerializer,  # type: ignore
                         responses={201: user_response})
    def post(self, request: Request) -> Response:
        data = request.data
        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response(
                {
                    **serializer.errors,
                    "message": "Invalid request data",
                },
                status.HTTP_400_BAD_REQUEST
            )
        user = serializer.save()
        return Response(
            {
                "user": serializer.data,
                "message": "Registration was succsfull."
            },
            status.HTTP_201_CREATED
        )


class LoginAPIView(APIView):  # type: ignore
    serializer_class = LoginSerializer

    @swagger_auto_schema(request_body=LoginSerializer,  # type: ignore
                         responses={200: login_response})
    def post(self, request: Request) -> Response:
        data = request.data
        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response(
                {
                    **serializer.errors,
                    "message": "Invalid credentials."
                },
                status.HTTP_400_BAD_REQUEST
            )

        user = authenticate(**serializer.data)
        if not user:
            return Response(
                {
                    **serializer.errors,
                    "message": "Invalid credentials."
                },
                status.HTTP_400_BAD_REQUEST
            )

        token = jwt_encode_handler(jwt_payload_handler(user))
        return Response(
            {
                "message": "Successfully logged in.",
                "token": token
            },
            status.HTTP_200_OK
        )
