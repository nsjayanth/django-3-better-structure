from django.contrib.auth import get_user_model
from django.test import TestCase
from django.contrib.auth import authenticate

from .backends import EmailAuthenticate

# Create your tests here.


class UserManagerTest(TestCase):  # type: ignore
    def test_create_user(self) -> None:
        User = get_user_model()
        user = User.objects.create_user(email="normal@user.com", password="foo")
        self.assertEqual(user.email, 'normal@user.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(user.username)
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email='')
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password="foo")

    def test_create_superuser(self) -> None:
        User = get_user_model()
        admin_user = User.objects.create_superuser(email="super@user.com", password="foo")
        self.assertEqual(admin_user.email, 'super@user.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)

        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass
        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email='super@user.com', password='foo', is_superuser=False)

    def test_authenticate(self) -> None:
        User = get_user_model()
        User.objects.create_user(email="normal@user.com", password="foo")

        authenticated_user = authenticate(email="normal@user.com", password="foo")
        self.assertEqual("normal@user.com", authenticated_user.email)
        self.assertTrue(authenticated_user.is_active)

        self.assertIsNone(authenticate(email="normal@user.com", password="foo1"))

        self.assertIsNone(authenticate(email="wrong@user.com", password="foo"))

    def test_get_user(self) -> None:
        User = get_user_model()
        created_user = User.objects.create_user(email="normal@user.com", password="foo")
        self.assertIsNone(EmailAuthenticate.get_user(100))
        self.assertEqual(1, created_user.id)
        self.assertEqual("normal@user.com", created_user.email)
