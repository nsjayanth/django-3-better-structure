from typing import List
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .managers import CustomUserManager

from apps.common.models import BaseCustomModel

# Create your models here.


class CustomUser(BaseCustomModel, AbstractBaseUser, PermissionsMixin):  # type: ignore
    email: str = models.EmailField(_('email address'), unique=True)
    is_staff: bool = models.BooleanField(default=False)
    is_active: bool = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS: List[str] = []

    objects = CustomUserManager()

    def __str__(self) -> str:
        return self.email
