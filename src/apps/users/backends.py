import jwt
from typing import Any, Dict, List, Tuple, Optional
from django.contrib.auth import get_user_model
from django.utils.encoding import smart_text
from rest_framework import authentication
from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework_jwt.settings import api_settings
from rest_framework import exceptions
from django.utils.translation import ugettext_lazy as _
from rest_framework.request import Request

from .models import CustomUser as User

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class EmailAuthenticate:
    """Email authenticate class
    """
    @staticmethod
    def authenticate(email: str, password: str) -> Optional[User]:
        """email and password authentication"""

        try:
            user: User = User.objects.get(email=email)
            if user.check_password(password):
                return user
            return None
        except User.DoesNotExist:
            return None
        except Exception as e:
            return None

    @staticmethod
    def get_user(user_id: int) -> Optional[User]:
        """get user by id"""
        try:
            user: User = User.objects.get(id=user_id)
            if user.is_active:
                return user
            return None
        except User.DoesNotExist:
            return None
        except Exception as e:
            return None


class BaseJSONWebTokenAuthenticate(BaseAuthentication):  # type: ignore
    """
    Token based authentication using the JSON Web Token standard.
    """

    def authenticate(self, request: Request) -> Optional[Tuple[User, Dict[str, Any]]]:
        """
        Returns a tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            return None

        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = _("Signature has expired.")
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = _("Error decoding signature.")
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)
        return user, payload

    def authenticate_credentials(self, payload: Dict[str, Any]) -> User:
        """
        Returns an active user that matches the payload's user id and email.
        """
        username = jwt_get_username_from_payload(payload)
        if not username:
            msg = _("Invalid payload")
            raise exceptions.AuthenticationFailed(msg)

        try:
            user: User = User.objects.get_by_natural_key(username)
        except User.DoesNotExist:
            msg = _("Invalid signature.")
            raise exceptions.AuthenticationFailed(msg)

        if not user.is_active:
            msg = _("User account is disabled.")
            raise exceptions.AuthenticationFailed(msg)

        return user


class JSONWebTokenAuthenticate(BaseJSONWebTokenAuthenticate):
    """
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string specified in the setting
    `JWT_AUTH_HEADER_PREFIX`. For example:
        Authorization: JWT eyJhbGciOiAiSFMyNTYiLCAidHlwIj
    """
    www_authenticate_realm = "api"

    def get_jwt_value(self, request: Request) -> Optional[str]:
        auth: List[str] = get_authorization_header(request).split()
        auth_header_prefix = api_settings.JWT_AUTH_HEADER_PREFIX.lower()

        if not auth:
            if api_settings.JWT_AUTH_COOKIE:
                jwt_value: str = request.COOKIES.get(api_settings.JWT_AUTH_COOKIE)
                return jwt_value
            return None

        if smart_text(auth[0].lower()) != auth_header_prefix:
            return None

        if len(auth) == 1:
            msg = _("Invalid Authorization header. No credentials provided.")
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _(
                "Invalid Authorization header. Credentials string "
                "should not contain spaces."
            )
            raise exceptions.AuthenticationFailed(msg)
        return auth[1]

    def authenticate_header(self, request: Request) -> str:
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        return f"{api_settings.JWT_AUTH_HEADER_PREFIX, self.www_authenticate_realm}"
