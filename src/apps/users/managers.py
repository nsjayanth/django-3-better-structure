from typing import Any, Dict

from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _

User = get_user_model


class CustomUserManager(BaseUserManager):  # type: ignore
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """

    def create_user(self, email: str, password: str,
                    **extra_fields: Dict[str, Any]) -> User:  # type: ignore
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_("The email must be set"))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user  # type: ignore

    def create_superuser(self, email: str, password: str, **extra_fields: Dict[str, Any]) -> User:  # type: ignore
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)  # type: ignore
        extra_fields.setdefault('is_superuser', True)  # type: ignore
        extra_fields.setdefault('is_active', True)  # type: ignore

        if extra_fields.get('is_staff') is not True:  # type: ignore
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:  # type: ignore
            raise ValueError(_('Superuser must have is_superuser=True.'))

        return self.create_user(email, password, **extra_fields)
