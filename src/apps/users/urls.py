from django.conf.urls import url

from .views import CreateUserAPIView, LoginAPIView

urlpatterns = [
    url(r"^register", CreateUserAPIView.as_view(), name="create_user"),
    url(r"^login", LoginAPIView.as_view(), name="login_user"),
]
