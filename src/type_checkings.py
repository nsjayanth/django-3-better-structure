from django.conf import settings
import glob
import os
import sys
from typing import List

BASE_DIR = settings.BASE_DIR

args = ['--cache-dir', os.path.join(BASE_DIR, '.mypy_cache'),
        '--check-untyped-defs',
        '--disallow-untyped-calls',
        '--disallow-untyped-defs',
        '--disallow-incomplete-defs',
        '--ignore-missing-imports',
        '--show-error-context',
        '--show-column-numbers',
        '--strict-equality',
        '--strict',
        '--warn-redundant-casts',
        '--warn-return-any',
        '--warn-unreachable',
        '--warn-unused-ignores']


def _get_python_files() -> List[str]:
    ignored_dirs = [
        ".mypy_cache", ".venv", ".vscode", "migrations"
    ]
    ignored_modules = [
        "manage.py"
    ]
    python_files = []
    for root in os.scandir(BASE_DIR):
        root_name = root.name
        if root_name not in [*ignored_dirs, *ignored_modules]:
            if not root.is_dir():
                if root_name.endswith(".py"):
                    python_files.append(root_name)
            else:
                python_files.extend(glob.glob(f"{root_name}/[!migrations]**/*.py",
                                              recursive=True))
    return python_files


def start_type_checking() -> int:
    try:
        from mypy import main as mypy_main
        mypy_main.main(
            script_path=None,
            stdout=sys.stdout,
            stderr=sys.stderr,
            args=args + _get_python_files()
        )
    except SystemExit as err:
        msg = f"return code: {err.code}"
        sys.stdout.write(f"Failed to execute mypy, {msg}")
        return 1
    return 0


if __name__ == "__main__":
    start_type_checking()
