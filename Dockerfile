FROM python:3.7

RUN mkdir /app
# RUN mkdir -p /app/var/logs

COPY . /app

WORKDIR /app/src

ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN pip3 install poetry
RUN poetry config virtualenvs.create false --local
RUN poetry config virtualenvs.in-project false --local

RUN poetry install --no-dev

ENV DEBUG_LOG_FILE="../var/logs/main_debug.log"
ENV PRODUCTION_LOG_FILE="../var/logs/main.log"

EXPOSE 8000

ENTRYPOINT ["/app/docker-entrypoint.sh"]

CMD [ "gunicorn", "--bind", ":8000", "default_app.wsgi:application", "--reload"]
