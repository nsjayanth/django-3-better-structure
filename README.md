### Django better structure

##### packages/dependencies

- [django=3.2](https://docs.djangoproject.com/en/3.2/)
- [djangorestframework](https://www.django-rest-framework.org/)
- [poetry](https://python-poetry.org/)
- [PyJWT](https://pyjwt.readthedocs.io/en/latest/)
- [MyPy](https://mypy.readthedocs.io/en/stable/)
- [drf-yasg](https://drf-yasg.readthedocs.io/en/stable/)
- [python-dotenv](https://pypi.org/project/python-dotenv/)
- [pycodestyle](https://pypi.org/project/pycodestyle/)
- [coverage](https://coverage.readthedocs.io/en/latest/index.html)

##### Install dependencies

```bash
poetry install
```

NOTE: first time it creates a virtual environemnt locally

##### Activate the environment using Poetry

```bash
poetry shell
```

#### Root directory is src so move insde the src folder

##### Create a required files

- create `.env` file and store the values for the below env variables
  ```bash
  DEBUG_LOG_FILE="<degub_log_file_path>.log"
  PRODUCTION_LOG_FILE="<production_log_file_path>.log"
  ```

##### Run migrations

```bash
python manage.py makemigrations
```

##### Apply the migrations

```bash
python manage.py migrate
```

##### to run the server

```bash
python manage.py runserver
```

## ----------Happy Coding---------------
